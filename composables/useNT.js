import { useStorage } from "@vueuse/core";

export function useBasePath(path = "") {
  const config = useRuntimeConfig();
  return config.public.baseurl + path?.toString();
}
const   getToken= ()=> {
  const state =  useStorage("tokenstr",null);  
  return state.value;  
}
export function useCheckLogin(t = "") {
  const state = useStorage("tokenstr", null);
  if (t) {
    state.value = t;
  }

  if (state.value) {
    let decoded = decodeJWT(state.value);
    const currentTime = Math.floor(Date.now() / 1000);
    const isExp = decoded.exp < currentTime;
    decoded.isExp = isExp;
    return decoded;
  }else{
    return "";
  }
  
}
export async function useGetProfile() {
  const config = useRuntimeConfig();
  const { data, pending, error } = await useFetch(config.public.getprofileUrl, {
    method: "GET",
    headers: { authorization: "Bearer " + getToken(),'appkey':config.public.hrdPublic,'Content-Type':'application/json' },
    initialCache: false,
  });
  return { data, pending, error };
 
}
export function useGetLoginPage() {
  const config = useRuntimeConfig();
  const loginPage=`${config.public.authUrl}?appkey=${config.public.hrdPublic}`;
  return loginPage;
}
export async function useDummySignin(user) {
  const { data, pending, error } = await useFetch("/api/dummy", {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: user,
    initialCache: false,
  });
  //console.log('useDummySignin',data._value.data);
  useStorage("tokenstr", data._value.data);
  navigateTo("/");
}

export function useLogout() {
  const token = useStorage("tokenstr", null);
  token.value = null;
}

function decodeJWT(token) {
  try {
    const base64Url = token.split(".")[1];
    const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
    return JSON.parse(window.atob(base64));
  } catch (error) {
    return '';
  }
}
