import jwt from "jsonwebtoken";
const jwtsecret='data';
export async function jwtVerify(token) {
  return jwt.verify(token, jwtsecret);
}
export const signToken = (user) => {
  return jwt.sign({ data: user }, jwtsecret, {
    expiresIn: 1296000,
  });
};
export async function getUserJwt(token) {
  var r;
  var user=  jwt.verify(token, jwtsecret);
  //console.log('Data ',token,user);
  const p = await Promise.resolve(user);  
  return p.data;
}

