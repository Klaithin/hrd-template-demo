// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig(  
  {
  modules: [
    "@nuxtjs/tailwindcss",
  ],
  
  runtimeConfig: {
    jwtSecret: process.env.JWT_SECRET, // NUXT_JWT_SECRET (private)
    mariaDb: process.env.MARIA_DB_NAME, 
    mariaUsername: process.env.MARIA_USERNAME, 
    mariaPassword: process.env.MARIA_PASSWORD, 
    public: {
      authUrl: process.env.AUTH_URL, 
      getprofileUrl: process.env.GETPROFILE_URL, 
      baseurl: process.env.BASE_URL, 
      localKey: process.env.LOCAL_KEY,
    }
  },
  app: {
    baseURL: process.env.BASE_URL,     
    head: {
      title: "สถาบันวิชาการ",
    }
  },
  
})

