/** @type {import('tailwindcss').Config} */
const colors = require("tailwindcss/colors");
module.exports = {
  content: [
    './src/**/*.{html,vue,js,ts,jsx,tsx}',
    './components/**/*.{html,js}',
    './pages/**/*.{html,js}',
    "./index.html", 
    "./node_modules/vue-tailwind-datepicker/**/*.js"
  ],
  theme: {
    extend: {
      colors: {
        "vtd-primary": colors.sky, // Light mode Datepicker color
        "vtd-secondary": colors.gray, // Dark mode Datepicker color
      },
    },
  },
  plugins: [
    require("daisyui",'@tailwindcss/forms')
  ],
  
}
